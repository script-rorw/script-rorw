import os, sys
# import time
import xbmc
import xbmcgui
from threading import Thread,Lock,Timer
import subprocess

_ = sys.modules[ "__main__" ].__language__
__scriptname__ = sys.modules[ "__main__" ].__scriptname__
__version__    = sys.modules[ "__main__" ].__version__
__addon__      = sys.modules[ "__main__" ].__addon__
__resource__   = sys.modules[ "__main__" ].__resource__
__cwd__        = sys.modules[ "__main__" ].__cwd__


PROC_MOUNTS='/proc/mounts'
FSTAB_LINK='/fstab.rorw'
FSTAB_RO='/fstab.ro'
FSTAB_RW='/fstab.rw'

EXIT_SCRIPT = ( 9, 10, 247, 275, 61467, 92, )
CANCEL_DIALOG = EXIT_SCRIPT + ( 216, 257, 61448, )
ACTION_OK = 7

def CheckOsRw():
    rootRW=True
    
    # Reading a known file that must be there and must be readable, if not it is a permanent error,
    # Thus limiting excetp/fnally here.
    try:
	with open(PROC_MOUNTS, 'r') as fsrc:
	    lines = fsrc.readlines()
	    for line in lines:
		fields = line.split()
		if fields[0] != "rootfs" and fields[1] == '/':
		    options = fields[3].split(',')
		    if "ro" in options: rootRW = False
		    break
    except:
	return (False,False)

    return (True, rootRW)

def CheckOptionRw():
    return os.path.normpath(os.path.join(os.path.dirname(FSTAB_LINK), os.readlink(FSTAB_LINK))) == FSTAB_RW


def ModifyMarker(rw = False):

    if CheckOptionRw() == rw: return True
    
    # If any operation fails, there is a serious problem.
    # Thus limiting excetp/fnally here.
    try:
	status = CheckOsRw()
	if status[0] == False:
	    return False

	if status[1] == False:
	    if os.system("sudo /bin/mount -o remount,rw /") != 0: return False
	
	if rw == True:
	    if os.system("echo 'ln -sf %s %s' | sudo -s" % (FSTAB_RW, FSTAB_LINK)) != 0: return False
	else:
	    if os.system("echo 'ln -sf %s %s' | sudo -s" % (FSTAB_RO, FSTAB_LINK)) != 0: return False
	
	if status[1] == False:
	    if os.system("sudo /bin/mount -o remount,ro /") != 0: return False
    except:
	return False
	
    return True


def getKeyboard(default = "", heading = "", hiden = False):
    keyboard = xbmc.Keyboard(default, heading)
    try:
	if hiden:
	    keyboard.setHiddenInput( True )
	keyboard.doModal()
	if (keyboard.isConfirmed()):
	    return keyboard.getText(), True
    except:
	pass
    return default, False

def setRORW(rw):
    if ModifyMarker(rw) == False:
	xbmcgui.Dialog().ok("Error","Cannot apply required settings.")
	return False
    # xbmcgui.Dialog().ok("Status","On next startup system will be mounted %s.\nReboot to take effect." % ("read-write" if rw else "read-only"))
    return True;

def SystemReady():
    # Just simple check
    return os.path.islink(FSTAB_FILE) and (os.path.normpath(os.path.join(os.path.dirname(FSTAB_FILE), os.readlink(FSTAB_FILE))) == FSTAB_LINK)

class GUI( xbmcgui.WindowXMLDialog ):
    def __init__( self, *args, **kwargs ):
      xbmcgui.WindowXMLDialog.__init__(self)
      pass

    def onInit( self ):
      status = CheckOsRw()
      if status[0] == False:
        self.getControl(104).setLabel(_(32007))
      else:
        if status[1] == True:
	    self.getControl(104).setLabel(_(32001))
        else:
	    self.getControl(104).setLabel(_(32000))

      status = CheckOptionRw()
      if status == True:
	self.getControl(106).setLabel(_(32001))
      else:
	self.getControl(106).setLabel(_(32000))

    def debug(self,msg):
      xbmc.log("### [%s] - %s" % (__scriptname__,msg,),level=xbmc.LOGDEBUG )

    def exit_script( self, restart=False ):
      self.close()

    def onClick( self, controlId ):
      self.debug("Click: [%i]" % controlId)
      if controlId == 100:
	if setRORW(False):
	    self.getControl(106).setLabel(_(32000))
      elif controlId == 101:
	if setRORW(True):
	    self.getControl(106).setLabel(_(32001))
      else:
        self.exit_script()

    def onFocus( self, controlId ):
       self.controlId = controlId
       pass

    def onAction( self, action ):
       self.debug("Button: [%s] Action: [%s]" % (action.getButtonCode(), action.getId()))
       button=action.getButtonCode()
       if ( button in CANCEL_DIALOG ):
          self.exit_script()


###################### Installer part #######################

FSTAB_RO_CONTENT="""
/dev/mmcblk0p1 /boot vfat defaults,noatime,noauto,x-systemd.automount,ro 0 0
/dev/mmcblk0p2 / ext4 defaults,noatime,ro 0 0
overlay /etc rorw defaults 0 0
overlay /var rorw defaults 0 0
overlay /home rorw defaults 0 0
tmpfs /tmp tmpfs defaults,mode=1777,strictatime,nosuid,nodev 0 0
tmpfs /media tmpfs defaults 0 0
"""

RORW_MOUNT_FILE_CONTENT="""#!/bin/bash
[ -d "${2}_overlay" ] || exit 1
/bin/mount -t tmpfs tmpfs "${2}_overlay"
mkdir "${2}_overlay/upperdir"
mkdir "${2}_overlay/workdir"
/bin/mount -t overlay overlay -olowerdir="${2}",upperdir="${2}_overlay/upperdir",workdir="${2}_overlay/workdir" "$2"
"""

TEMP_DIR = xbmc.translatePath('special://temp/')
TMP_FILE = xbmc.translatePath(os.path.join(TEMP_DIR, 'rorw.tmp'))
FSTAB_FILE='/etc/fstab'
README_FILE = os.path.join(__cwd__, 'README.md')
RORW_MOUNT_FILE='/sbin/mount.rorw'
HOME_OVERLAY_DIR='/home_overlay'
ETC_OVERLAY_DIR='/etc_overlay'
VAR_OVERLAY_DIR='/var_overlay'
OS_RELEASE_FILE='/etc/os-release'
OS_RELEASES=("2017.12-1")

'''
OS_RLEASE_FILE='/etc/os-release'
OS_RELEASES=("2017.12-1",) #in this tmp.mount was removed

def CheckRelease():
    name = False
    release = -1
    try:
        with open(OS_RELEASE_FILE) as f:
            for l in f:
                try:
                    n,v=l.strip().split('=')
                    print "n:%s, v:%s" % (n,v)
                    if n == "NAME" and v.replace('"','') == "OSMC":
                        name = True
                    if n == "VERSION_ID":
                        release=0
                        r = time.strptime(v.replace('"',''), "%Y.%m-%d")
                        for t in OS_RELEASES:
                            if time.strptime(t, "%Y.%m-%d") > r:
                                break
                            release += 1
                except:
                    pass
    except:
        pass
    return release if name == True else -1
'''

def CheckOverlayPresence():
    if os.system("sudo modprobe overlay") != 0: return False
    return True

def InstallFiles():
    try:
	if not os.path.isdir(HOME_OVERLAY_DIR):
	    if os.system("sudo /bin/mkdir %s" % HOME_OVERLAY_DIR) != 0: return False
	if not os.path.isdir(ETC_OVERLAY_DIR):
	    if os.system("sudo /bin/mkdir %s" % ETC_OVERLAY_DIR) != 0: return False
	if not os.path.isdir(VAR_OVERLAY_DIR):
	    if os.system("sudo /bin/mkdir %s" % VAR_OVERLAY_DIR) != 0: return False
    
	if os.system("sudo cp -f %s %s" % (FSTAB_FILE, FSTAB_RW)) != 0: return False
	
	with open(TMP_FILE, "w") as f:
	    f.write(FSTAB_RO_CONTENT)
	if os.system("sudo mv -f %s %s" % (TMP_FILE, FSTAB_RO)) != 0: return False

	with open(TMP_FILE, "w") as f:
	    f.write(RORW_MOUNT_FILE_CONTENT)
	if os.system("sudo mv -f %s %s" % (TMP_FILE, RORW_MOUNT_FILE)) != 0: return False
	if os.system("sudo chmod 4755 %s" % RORW_MOUNT_FILE) != 0: return False

	if os.system("echo 'ln -sf %s %s' | sudo -s" % (FSTAB_RW, FSTAB_LINK)) != 0: return False
	
	if os.system("echo 'ln -sf %s %s' | sudo -s" % (FSTAB_LINK, FSTAB_FILE)) != 0: return False

    except:
	return False
    return True
    
def GetReadme():
    text = "\n\nREADME:\n"
    with open(README_FILE, 'r') as fsrc:
	text += ''.join(fsrc.readlines())
    return text

class InstallerGUI( xbmcgui.WindowXMLDialog ):
    installer = None
    
    def __init__( self, *args, **kwargs ):
      xbmcgui.WindowXMLDialog.__init__(self)
      pass

    def onInit( self ):
      if CheckOverlayPresence():
        text = _(32102) + GetReadme()
      else:
        text = _(32101)
        self.getControl(100).setEnabled(False)
      self.getControl(102).setText(text)
      pass

    def debug(self,msg):
      xbmc.log("### [%s] - %s" % (__scriptname__,msg,),level=xbmc.LOGDEBUG )

    def onClick( self, controlId ):
      self.debug("Click: [%i]" % controlId)
      if controlId == 100:
        if InstallFiles():
          text = _(32112)
        else:
          text = _(32104)
        self.getControl(100).setEnabled(False)
        self.getControl(102).setText(text)
      pass

    def onFocus( self, controlId ):
       self.controlId = controlId
       pass

    def onAction( self, action ):
       self.debug("Button: [%s] Action: [%s]" % (action.getButtonCode(), action.getId()))
       button=action.getButtonCode()
       if ( button in CANCEL_DIALOG ):
            self.close()
       pass

