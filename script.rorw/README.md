This script is a GUI to change the next system sartup in read-only or read-write mode.
Thus any settings chnages can be made parmanent in the latter case,
whilst there is no problem with FS corruption on unexpected power-off in the former case.

For this to work the system must be first prepared.
It can be done automatically with the help of the script itself or with few steps manually,
in case of any problems with automatic installer.

Sources:
https://gitlab.com/script-rorw/script-rorw.git

1. OSMC must be first installed and run normally (if previous version
   of script-rorw is installed it must be removed manually reverting
   system to initial state)


2. Install this script.rorw to OSMC and run it.



Alternate manul installation:
1. Login (ssh, user:osmc, pass:osmc) to running OSMC:


2. Create structure:
mkdir /etc_overlay
mkdir /home_overlay
mkdir /var_overlay
cp -f /etc/fstab /fstab.rw

cat > /fstab.ro << EOF
/dev/mmcblk0p1 /boot vfat defaults,noatime,noauto,x-systemd.automount,ro 0 0
/dev/mmcblk0p2 / ext4 defaults,noatime,ro 0 0
overlay /etc rorw defaults 0 0
overlay /var rorw defaults 0 0
overlay /home rorw defaults 0 0
tmpfs /tmp tmpfs defaults,mode=1777,strictatime,nosuid,nodev 0 0
tmpfs /media tmpfs defaults 0 0
EOF

cat > /sbin/mount.rorw << EOF
#!/bin/bash
[ -d "${2}_overlay" ] || exit 1
/bin/mount -t tmpfs tmpfs "${2}_overlay"
mkdir "${2}_overlay/upperdir"
mkdir "${2}_overlay/workdir"
/bin/mount -t overlay overlay -olowerdir="${2}",upperdir="${2}_overlay/upperdir",workdir="${2}_overlay/workdir" "$2"
EOF

chmod 4755 /sbin/mount.rorw

ln -fs /fstab.rorw /etc/fstab

# to run in rw mode do:
ln -sf /fstab.rw /fstab.rorw
# to run in ro mode do:
ln -sf /fstab.ro /fstab.rorw

3. reboot OSMC
